/**
 * @file
 * Adds additional commands to the standard Drupal AJAX functionality.
 */

(function ($) {

/**
 * Command to set the window.location, redirecting the browser.
 */
Drupal.ajax.prototype.commands.redirect = function (ajax, response, status) {
  window.location.href = response.url;
};

/**
 * Command to trigger window.location.reload(), reloading the current page.
 */
Drupal.ajax.prototype.commands.reload = function (ajax, response, status) {
  window.location.reload();
};

})(jQuery);
