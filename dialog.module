<?php

/**
 * @file
 * Provides an API for opening content in a dialog.
 */

/**
 * URL query attribute to indicate the wrapper used to render a request.
 *
 * The wrapper format determines how the HTML is wrapped, for example in a
 * modal dialog.
 */
define('DIALOG_WRAPPER_FORMAT', '_format');

// AJAX commands.
require_once dirname(__FILE__) . '/includes/dialog.commands.inc';

/**
 * Implements hook_module_implements_alter().
 */
function dialog_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'library_alter') {
    // Move hook_library_alter() to the end of the list in order to run after
    // jquery_update.
    // module_implements() iterates through $implementations with a foreach loop
    // which PHP iterates in the order that the items were added, so to move an
    // item to the end of the array, we remove it and then add it.
    $group = $implementations ['dialog'];
    unset($implementations ['dialog']);
    $implementations ['dialog'] = $group;
  }
}

/**
 * Implements hook_library().
 */
function dialog_library() {
  $libraries['drupal.dialog'] = array(
    'title' => 'Drupal Dialog',
    'website' => 'http://www.drupal.org',
    'version' => VERSION,
    'js' => array(
      drupal_get_path('module', 'dialog') . '/js/dialog/dialog.js' => array(
        'group' => JS_LIBRARY,
        'weight' => 2,
      ),
      drupal_get_path('module', 'dialog') . '/js/dialog/dialog.position.js' => array(
        'group' => JS_LIBRARY,
        'weight' => 2,
      ),
      drupal_get_path('module', 'dialog') . '/js/dialog/dialog.jquery-ui.js' => array(
        'group' => JS_LIBRARY,
        'weight' => 2,
      ),
    ),
    'css' => array(
      drupal_get_path('module', 'dialog') . '/css/components/ui-dialog.css' => array(),
      drupal_get_path('module', 'dialog') . '/js/dialog/dialog.theme.css' => array(),
    ),
    'dependencies' => array(
      array('system', 'jquery'),
      array('system', 'ui.dialog'),
      array('dialog', 'drupal.debounce'),
      array('dialog', 'drupal.displace'),
    ),
  );

  $libraries['drupal.dialog.ajax'] = array(
    'title' => 'Drupal AJAX Dialog',
    'website' => 'http://www.drupal.org',
    'version' => VERSION,
    'js' => array(
      drupal_get_path('module', 'dialog') . '/js/dialog/dialog.ajax.js' => array(
        'group' => JS_LIBRARY,
        'weight' => 2,
      ),
    ),
    'dependencies' => array(
      array('system', 'jquery'),
      array('system', 'drupal.ajax'),
      array('dialog', 'drupal.dialog'),
    ),
  );

  $libraries['drupal.displace'] = array(
    'title' => 'Drupal Displace',
    'website' => 'http://www.drupal.org',
    'version' => VERSION,
    'js' => array(
      drupal_get_path('module', 'dialog') . '/js/drupal/displace.js' => array(
        'group' => JS_LIBRARY,
      ),
    ),
    'dependencies' => array(
      array('system', 'jquery'),
      array('dialog', 'drupal.debounce'),
    ),
  );

  $libraries['drupal.debounce'] = array(
    'title' => 'Drupal Debounce',
    'website' => 'http://www.drupal.org',
    'version' => VERSION,
    'js' => array(
      drupal_get_path('module', 'dialog') . '/js/drupal/debounce.js' => array(
        'group' => JS_LIBRARY,
      ),
    ),
  );

  // Register libraries on behalf of classy.theme.
  $libraries['classy.drupal.dialog'] = array(
    'title' => 'Classy Drupal Dialog',
    'website' => 'http://www.drupal.org',
    'version' => VERSION,
    'css' => array(
      drupal_get_path('module', 'dialog') . '/themes/classy/css/components/buttons.css' => array(),
      drupal_get_path('module', 'dialog') . '/themes/classy/css/components/dialog.css' => array(),
      drupal_get_path('module', 'dialog') . '/themes/classy/css/components/ui-dialog.css' => array(),
    ),
  );

  // Register libraries on behalf of bartik.theme.
  $libraries['bartik.drupal.dialog'] = array(
    'title' => 'Bartik Drupal Dialog',
    'website' => 'http://www.drupal.org',
    'version' => VERSION,
    'css' => array(
      drupal_get_path('module', 'dialog') . '/themes/bartik/css/components/buttons.css' => array(),
      drupal_get_path('module', 'dialog') . '/themes/bartik/css/components/ui-dialog.css' => array(),
    ),
    'dependencies' => array(
      array('dialog', 'classy.drupal.dialog'),
    ),
  );

  // Register libraries on behalf of seven.theme.
  $libraries['seven.ckeditor-dialog'] = array(
    'title' => 'Custom CKEditor dialog styling for the Seven theme.',
    'version' => VERSION,
    'css' => array(
      drupal_get_path('module', 'dialog') . '/themes/seven/css/theme/ckeditor-dialog.css' => array(),
    ),
  );

  $libraries['seven.drupal.dialog'] = array(
    'title' => 'Seven Drupal Dialog',
    'website' => 'http://www.drupal.org',
    'version' => VERSION,
    'css' => array(
      drupal_get_path('module', 'dialog') . '/themes/seven/css/components/buttons.css' => array(),
      drupal_get_path('module', 'dialog') . '/themes/seven/css/components/dialog.css' => array(),
    ),
  );

  $libraries['seven.jquery.ui'] = array(
    'title' => 'Seven jQuery UI',
    'website' => 'http://www.drupal.org',
    'version' => VERSION,
    'css' => array(
      drupal_get_path('module', 'dialog') . '/themes/seven/css/components/jquery.ui/theme.css' => array(
        'weight' => -1,
      ),
    ),
  );

  // Register libraries on behalf of toolbar.module.
  $libraries['toolbar.toolbar'] = array(
    'title' => 'Toolbar',
    'website' => 'http://www.drupal.org',
    'version' => VERSION,
    'css' => array(
      drupal_get_path('module', 'toolbar') . '/toolbar.css',
      drupal_get_path('module', 'toolbar') . '/toolbar-print.css' => array(
        'media' => 'print',
      ),
    ),
    'js' => array(
      drupal_get_path('module', 'dialog') . '/js/toolbar/toolbar.js' => array(),
      array(
        'data' => array('tableHeaderOffset' => 'Drupal.toolbar.height'),
        'type' => 'setting'
      ),
    ),
    'dependencies' => array(
      array('system', 'jquery'),
      array('system', 'jquery.cookie'),
      array('dialog', 'drupal.displace'),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_library_alter().
 */
function dialog_library_alter(&$libraries, $module) {
  // Replace the default implementation of misc/ajax.js and add additional
  // functionality.
  if ($module == 'system' && isset($libraries['drupal.ajax'])) {
    $path = 'misc/ajax.js';
    unset($libraries['drupal.ajax']['js'][$path]);

    $libraries['drupal.ajax']['js'][drupal_get_path('module', 'dialog') . '/misc/ajax.js'] = array('group' => JS_LIBRARY, 'weight' => 2);
    $libraries['drupal.ajax']['js'][drupal_get_path('module', 'dialog') . '/misc/ajax.dialog.js'] = array('group' => JS_LIBRARY, 'weight' => 2);
  }

  global $theme;

  // Alter libraries on behalf of bartik.theme.
  if ($theme == 'bartik') {
    // Replace the default dialog theme CSS with custom CSS for bartik.theme.
    if ($module == 'dialog' && isset($libraries['drupal.dialog'])) {
      $path = drupal_get_path('module', 'dialog') . '/js/dialog/dialog.theme.css';
      unset($libraries['drupal.dialog']['css'][$path]);

      $libraries['drupal.dialog']['dependencies'][] = array('dialog', 'bartik.drupal.dialog');
    }
  }

  // Alter libraries on behalf of seven.theme.
  if ($theme == 'seven') {
    // Replace the default jQuery UI theme CSS with custom CSS for seven.theme.
    if ($module == 'system' && isset($libraries['ui'])) {
      $path = 'misc/ui/jquery.ui.theme.css';
      unset($libraries['ui']['css'][$path]);

      $libraries['ui']['dependencies'][] = array('dialog', 'seven.jquery.ui');
    }

    // Remove the default dialog styling.
    if ($module == 'system' && isset($libraries['ui.dialog'])) {
      $path = 'misc/ui/jquery.ui.dialog.css';
      unset($libraries['ui.dialog']['css'][$path]);
    }

    // Replace the default dialog theme CSS with custom CSS for seven.theme.
    if ($module == 'dialog' && isset($libraries['drupal.dialog'])) {
      $path = drupal_get_path('module', 'dialog') . '/js/dialog/dialog.theme.css';
      unset($libraries['drupal.dialog']['css'][$path]);

      $libraries['drupal.dialog']['dependencies'][] = array('dialog', 'seven.drupal.dialog');
    }

    // Add custom styling for CKEditor dialogs.
    if ($module == 'editor_ckeditor' && isset($libraries['ckeditor'])) {
      $libraries['ckeditor']['dependencies'][] = array('dialog', 'seven.ckeditor-dialog');
    }
  }

  // Require the dialog library when quick editing.
  if ($module == 'quickedit' && isset($libraries['quickedit'])) {
    $libraries['quickedit']['dependencies'][] = array('dialog', 'drupal.dialog');
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Add assets to the Views UI 'edit' page to fix styling issues until Dialog
 * replaces CTools for modal support.
 */
function dialog_form_views_ui_edit_form_alter(&$form, &$form_state, $form_id) {
  $form['#attached']['library'][] = array('dialog', 'drupal.dialog.ajax');
}

/**
 * Gets the normalized type of a request.
 *
 * The normalized type is a short, lowercase version of the format, such as
 * 'html', 'json' or 'atom'.
 *
 * @return string|bool
 *   The normalized type of a given request as a string or FALSE if a format
 *   could not be detected.
 */
function dialog_get_content_type() {
  $query_parameters = drupal_get_query_parameters();

  return isset($query_parameters[DIALOG_WRAPPER_FORMAT]) ? $query_parameters[DIALOG_WRAPPER_FORMAT] : FALSE;
}

/**
 * Determines if the current request is via AJAX.
 *
 * @return bool
 *   TRUE if the current request is via AJAX, FALSE otherwise.
 */
function dialog_is_ajax() {
  $content_type = dialog_get_content_type();
  $wrapper_format = isset($content_type) ? $content_type : '';

  foreach (array('drupal_ajax', 'drupal_modal', 'drupal_dialog') as $wrapper) {
    if (strpos($wrapper_format, $wrapper) !== FALSE) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Implements hook_page_delivery_callback_alter().
 *
 * Enables the ability to display arbitrary pages as dialogs based upon query
 * parameters.
 */
function dialog_page_delivery_callback_alter(&$delivery_callback, $page_output = array()) {
  $content_type = dialog_get_content_type();

  // For all ajax.js initiated requests, deliver as JSON.
  if ($content_type == 'drupal_ajax') {
    $delivery_callback = 'ajax_deliver';
  }
  // For links with the data-dialog attribute, deliver as dialog JSON.
  elseif ($content_type == 'drupal_dialog') {
    $delivery_callback = 'ajax_deliver_dialog';
  }
  elseif ($content_type == 'drupal_modal') {
    $delivery_callback = 'ajax_deliver_modal';
  }
  // If the page response is a set of AJAX commands, deliver as JSON.
  // @todo: Requires https://www.drupal.org/node/897504.
  elseif (isset($page_output['#type']) && $page_output['#type'] === 'ajax') {
    $delivery_callback = 'ajax_deliver';
  }
}

/**
 * Delivers the content of a page as a dialog.
 *
 * @param $page_callback_result
 *   The result of a page callback. Can be one of:
 *   - NULL: to indicate no content.
 *   - An integer menu status constant: to indicate an error condition.
 *   - A string of HTML content.
 *   - A renderable array of content.
 *
 * @return
 *   An Ajax commands array that can be passed to ajax_render().
 */
function ajax_deliver_dialog($page_callback_result) {
  if (!isset($page_callback_result)) {
    // Simply delivering an empty commands array is sufficient. This results
    // in the Ajax request being completed, but nothing being done to the page.
  }
  else {
    $title = drupal_get_title();
    $content = $page_callback_result;

    if ((is_int($page_callback_result)) ) {
      switch ($page_callback_result) {
        case MENU_NOT_FOUND:
          $title = t('Page not found');
          $content = t('The requested page could not be found.');
          break;

        case MENU_ACCESS_DENIED:
          $title = t('Access denied');
          $content = t('You are not authorized to access this page.');
          break;

        case MENU_SITE_OFFLINE:
          $title = t('Site under maintenance');
          $content = filter_xss_admin(variable_get('maintenance_mode_message',
            t('@site is currently under maintenance. We should be back shortly. Thank you for your patience.', array('@site' => variable_get('site_name', 'Drupal')))));
          break;
      }
    }
    elseif (is_array($page_callback_result)) {
      $content = drupal_render($page_callback_result);

      if (isset($page_callback_result['#title'])) {
        $title = $page_callback_result['#title'];
      }
    }

    // Determine the dialog options and the target for the OpenDialogCommand.
    $dialog_options = isset($_POST['dialogOptions']) ? $_POST['dialogOptions'] : array();
    $selector = dialog_determine_target_selector($dialog_options);

    // Convert string values to booleans.
    foreach ($dialog_options as $key => $value) {
      if ($value === 'true') {
        $dialog_options[$key] = TRUE;
      }
      elseif ($value === 'false') {
        $dialog_options[$key] = FALSE;
      }
    }

    $commands[] = dialog_command_open_dialog($selector, $title, $content, $dialog_options);
    $return = array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );

    ajax_deliver($return);
  }
}

/**
 * Delivers the content of a page as a modal.
 *
 * @param $page_callback_result
 *   The result of a page callback. Can be one of:
 *   - NULL: to indicate no content.
 *   - An integer menu status constant: to indicate an error condition.
 *   - A string of HTML content.
 *   - A renderable array of content.
 *
 * @return
 *   An Ajax commands array that can be passed to ajax_render().
 */
function ajax_deliver_modal($page_callback_result) {
  if (!isset($page_callback_result)) {
    // Simply delivering an empty commands array is sufficient. This results
    // in the Ajax request being completed, but nothing being done to the page.
  }
  else {
    $title = drupal_get_title();
    $content = $page_callback_result;

    if ((is_int($page_callback_result)) ) {
      switch ($page_callback_result) {
        case MENU_NOT_FOUND:
          $title = t('Page not found');
          $content = t('The requested page could not be found.');
          break;

        case MENU_ACCESS_DENIED:
          $title = t('Access denied');
          $content = t('You are not authorized to access this page.');
          break;

        case MENU_SITE_OFFLINE:
          $title = t('Site under maintenance');
          $content = filter_xss_admin(variable_get('maintenance_mode_message',
            t('@site is currently under maintenance. We should be back shortly. Thank you for your patience.', array('@site' => variable_get('site_name', 'Drupal')))));
          break;
      }
    }
    elseif (is_array($page_callback_result)) {
      $content = drupal_render($page_callback_result);

      if (isset($page_callback_result['#title'])) {
        $title = $page_callback_result['#title'];
      }
    }

    $dialog_options = isset($_POST['dialogOptions']) ? $_POST['dialogOptions'] : array('modal' => TRUE);

    // Convert string values to booleans.
    foreach ($dialog_options as $key => $value) {
      if ($value === 'true') {
        $dialog_options[$key] = TRUE;
      }
      elseif ($value === 'false') {
        $dialog_options[$key] = FALSE;
      }
    }

    $commands[] = dialog_command_open_modal_dialog($title, $content, $dialog_options);
    $return = array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );

    ajax_deliver($return);
  }
}

/**
 * Determine the target selector for the OpenDialogCommand.
 *
 * @param array &$options
 *   The 'target' option, if set, is used, and then removed from $options.
 *
 * @return string
 *   The target selector.
 */
function dialog_determine_target_selector(array &$options) {
  // Generate the target wrapper for the dialog.
  if (isset($options['target'])) {
    // If the target was nominated in the incoming options, use that.
    $target = $options['target'];
    // Ensure the target includes the #.
    if (substr($target, 0, 1) != '#') {
      $target = '#' . $target;
    }
    // This shouldn't be passed on to jQuery.ui.dialog.
    unset($options['target']);
  }
  else {
    // Generate a target based on the route id.
    $route_name = current_path();
    $target = '#' . drupal_html_id("drupal-dialog-$route_name");
  }
  return $target;
}

/**
 * Implements hook_page_alter().
 */
function dialog_page_alter(&$page) {
  if (isset($page['page_top']['toolbar'])) {
    // If the toolbar is available, add a pre-render function to attach the
    // toolbar as a library.
    $page['page_top']['toolbar']['#pre_render'][] = 'dialog_toolbar_pre_render';
  }
}

/**
 * Pre-render function for attaching the toolbar as a library.
 */
function dialog_toolbar_pre_render($toolbar) {
  $toolbar['#attached']['library'][] = array('dialog', 'toolbar.toolbar');

  return $toolbar;
}

/**
 * Implements hook_js_alter().
 *
 * Replace JS files with dialog-enhanced versions.
 */
function dialog_js_alter(&$javascript) {
  $path = drupal_get_path('module', 'toolbar') . '/toolbar.js';

  // Remove toolbar.js. It will be replaced by a dialog-enhanced equivalent
  // included with the toolbar library.
  if (isset($javascript[$path])) {
    unset($javascript[$path]);
  }
}
